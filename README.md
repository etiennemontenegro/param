### Param
A interactive parameter system for micro-controller firmwares. Param aims to be useful and simple, allowing the developer to quickly add parameters throughout their code and interact with them in real time via a web interface ([WebParam](https://gitlab.com/maxd/webparam)), OSC, or even buttons and pots. 

Param is designed to not rely on platform specific code, it should be useful on any Arduino compatible device with a network connection. A param platform boiler plate like [Esparam](https://gitlab.com/maxd/esparam) for the ESP32 provides all the boiler plate to manage the network communication for param, along with saving and loading. It also provides a basic set of params to mannage the wifi settings and more. 

#### Parameters
Parameters are composed of a data type and an OSC style address.
Currently supported data types :
- bang
- bool
- int
- float
- color
- enum
- text

#### ParamCollector
Params are added to a `ParamCollector`, this list can be iterated over by other parts of the Param library.

```
IntParam myNumberParam;
BoolParam myBooleanParam;
ParamCollector paramCollector;

void setup(){
    myNumberParam.set("/device/myNumber", 0, 1024, 42);
    myBooleanParam.set("/device/myBoolean", 1);
    paramCollector.add(&myNumberParam);
    paramCollector.add(&myBooleanParam);
}
```

Technicaly we should be able to use multiple ParamCollectors if one ever wanted to separate groups of parameters to be handled in different ways.

### Base Param
All parameters contain a few things:
- OSC style address
- a value
- value changed callback
- value changed flag
- set with byte value
- a save condition
- input widget style (ie number box vs slider)

##### OSC address
The OSC address is set when initializing the parameter. The address is used to also create cascading menus, grouping parameters according to their path.
<!-- todo:insert image of cascading menues -->

##### Value Changed Callback and Flag
This calls a method in your code when the value of the param changes. Usefull when other code needs to be executed when the value changes. Alternatively the flag can be used to check if the value has changed.

```
IntParam myNumberParam;

void myNumberCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void setup(){
    myNumberParam.set("/device/number", 0, 10, 5);
    myNumberParam.setCallback(myNumberCallback);
}

void loop(){
    // Alternatively check the flag
    if(myNumberParam.checkFlag()){
        Serial.printf("int : %i\n", myNumberParam.v);
    }
}

```
##### Set with Byte Value
This allows simple inputs like midi/dmx/potentiometer to control the value of a param. With numerique params the value will be mapped across the range defined. With EnumParam if the number of options is below 25, they will be split by increments of 10 (enum option A 0-9, enum option B 10-19...). Currently ParamMappers cannot control color or text params as the data conversion is too lossy.

##### Save condition
The save condition is used by the saving and loading code.
- `NEVER_SAVE` is set by default
- `SAVE_ON_REQUEST` will only be saved when explicitely calling the save routine
- <s>`SAVE_ON_CHANGE` Save as soon as the value changes, not implemented... </s>

##### Input Widget Style
These are set by default, one may want to override the default with numerique parameters, to use a number box instead of a slider.

Numerical parameters can choose between
- `I_SLIDER` integer slider
- `F_SLIDER` float slider
- `NUMBER` number box

Other parameters dont really have a choice.
- `TOGGLE` toggle switch used by default for BoolParam
- `BANG` momentary push button
- `COLOR` color picker
- `TEXT` text field
- `OPTION` drop menu
<!-- - <s>`RADIO` could be nice for small enum ...</s> -->


#### Types of Params
##### BangParam
The simplest parameter, used to trigger some by sending a 'bang'.
##### BoolParam
The second simplest parameter, 0 or 1.
##### IntParam
Integer parameter, has a min, max, default fiedls.
`myInteger.set("/device/myint", 0, 100, 24);`
For webparam you can pick between a number box or a slider.

##### FloatParam
Same as IntParam but float value.

##### ColorParam
This is compatible with FastLED CRGB values.
`myColor.set("/device/mycol", CRGB(255,0,0));`
The webbrowsers have color pickers, this allows you to dial color with ease.
Via OSC, use seperate RGB values `/device/mycol 0 128 255`

##### EnumParam
Enumerated lists sure help make things clear when picking an option from a list. This will make a nice little drop menu option in the webparam. Via osc use exact value names `/device/myenum apple`.

```
// a enum to use with the EnumParam
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};

// EnumOption is used for the EnumParam.
EnumOption enumOptions[] = {
    {"first",FIRST},
    {"second", SECOND},
    {"third", THIRD}
};

EnumParam myEnumParam;

void setup(){
    myEnumParam.set("/example/enum", SECOND);
    myEnumParam.setOptions(enumOptions, sizeof(enumOptions) / sizeof(EnumOption));
}
```

##### TextParam
Simple write some text parameter. Uses a fixed size array of 32 characters which can be made larger if needed.
`#define TEXT_PARAM_MAX_LENGTH 64`

### webparam
webparam creates a simple UI based on parameters sent by a device. Using the same address patterns as OSC it nests the parameters together. You can access it at misc.nnvtn.ca/webparam.

#### OSC
Param supports OSC.
- prefix all addresses with device name?


### WebParam
*WebParam has moved to [https://gitlab.com/maxd/webparam](https://gitlab.com/maxd/webparam)* 
Websockets are a key component of Param, but has since been decoupled from this main library, as it requires librairies that are hardware specific. To avoid conflicts, webparam/websocket code is found in a separate repository. For ESP32 it is implemented in [Esparam](https://gitlab.com/maxd/esparam), for teensy and other platforms (Wiz850IO), scrounge some code from the examples until I make some sort of TeensyParam/ParamWiznet boilerplate.


### Utils
#### ParamJson
With the integration of the ArduinoJson library we can save and load parameters from various storage and parse incomming JSON messages.
- Has a `StaticJsonDocument`
    - size can be overwritten with `#define PARAM_JSON_BUFFER_SIZE 2048`

##### Save and Load
Saving and loading configs is done with ArduinoJson `serializeJson` and `deserializeJson`. See [esp32 example](examples/esp32_example/esp32_example.ino) for an example.


#### ParamLinker
ParamLinker is an object that allows connecting buttons/potentiometers to control a desired Param. The ParamLinker has a TextParam which holds the address of the parameter we wish to control. The linker can receive bang, boolean, and number inputs. Params have three receive functions, `receiveBang`, `receiveBool` and `receiveByte`, each parameter type will decide how to intrepet that input. For example, if IntParam receives a bang, it increments the value, but if it receives a bool, it toggles between min and default value. This allows to configure the behavior of buttons and pots on the fly. Currently ParamLinkers cannot control color or text params as the data conversion is too lossy.

See ParamLinker example.

### Examples
Only been keeping the ESP32 example up to date. It goes over most of what you can do with Param. 

### Development
- Currently re-writting WepParam to add some new features.
- I hope to support [OSCQueryProposal](https://github.com/vidvox/oscqueryproposal) in the future.
