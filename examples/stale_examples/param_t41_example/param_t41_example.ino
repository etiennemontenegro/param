/*
 * A websocket example for a board with wiznet ethernet connection
 *
 */


#include <NativeEthernet.h>
#include "Param.h"
#include "ParamCollector.h"
#include "ParamJson.h"
#include "ParamWebsockets.h"


#include "ArduinoJson.h"
// #include "OSCMessage.h"
// #include "SdFat.h"
// SdFatSdio sd;


#define CONFIG_FILENAME "CONFIG.JSO"

#define JSON_BUFFER_SIZE 1024
#define LED_PIN 13
// Define some parameters
FloatParam aFloatParam;
IntParam aIntParam;
BangParam aBangParam;
BoolParam aBoolParam;
ColorParam aColorParam;
TextParam aTextParam;
EnumParam aEnumParam;
BangParam saveParam;


// EnumParam is ment to work with an enum
enum THINGS {
    FIRST,
    SECOND,
    THIRD
};
// and an array of the values
const int enumValues[] = {FIRST, SECOND, THIRD};
// and the human readable strings
const char* enumStrings[] = {"first", "second", "third"};


// a param collector collects all the parameters
ParamCollector paramCollector;

void setup(){
    Serial.begin(9600);
    // the code waits for a serial connection!
    while(!Serial);
    Serial.println("[boot] connected via serial");
    pinMode(LED_PIN, OUTPUT);

    // set parameters
    aFloatParam.set("/a/float", 0, 1, 0.5);
    aFloatParam.saveType = SAVE_ON_REQUEST;
    aFloatParam.setCallback(floatCallback);

    aIntParam.set("/a/int", 0, 255, 100);
    aIntParam.saveType = SAVE_ON_REQUEST;
    aIntParam.setCallback(intCallback);

    aColorParam.set("/a/color", 0);
    aColorParam.saveType = SAVE_ON_REQUEST;
    aColorParam.setCallback(colorCallback);

    aBangParam.set("/a/bang");
    aBangParam.saveType = SAVE_ON_REQUEST;
    aBangParam.setCallback(bangFunction);

    aBoolParam.set("/a/bool", 0);
    aBoolParam.saveType = SAVE_ON_REQUEST;
    aBoolParam.setCallback(ledCallback);

    aTextParam.set("/a/text", "foo");
    aTextParam.saveType = SAVE_ON_REQUEST;
    aTextParam.setCallback(textCallback);

    aEnumParam.set("/a/enum", enumStrings, enumValues, 3, SECOND);
    aEnumParam.saveType = SAVE_ON_REQUEST;
    // aEnumParam.setCallback(enumCallback);

    saveParam.set("/params/save");
    saveParam.setCallback(saveConfig);
    Serial.println("[boot] params set");

    // add them in the collector
    paramCollector.add(&aFloatParam);
    paramCollector.add(&aIntParam);
    paramCollector.add(&aBangParam);
    paramCollector.add(&aBoolParam);
    paramCollector.add(&aColorParam);
    paramCollector.add(&aTextParam);
    paramCollector.add(&aEnumParam);
    paramCollector.add(&saveParam);
    Serial.println("[boot] params collected");

    // if (!sd.begin()) {
    //     Serial.println("sdcard initialization failed!");
    //     while (1);
    // }

    load(CONFIG_FILENAME);
    Serial.println("[boot] params loaded");

    byte mac[] = { 0x40, 0x19, 0xD3, 0xEF, 0x91, 0xDA };
    IPAddress ipAddress = {192,168,36,5};
    Ethernet.begin(mac, ipAddress);
    Serial.print("[boot] ethernet started at ");
    Serial.println(ipAddress);
    paramWebsocketInit(&paramCollector);
    Serial.println("[boot] done booting");

}

void loop(){
    updateParamWebsocket();
}

void update() {
    // do your loop() in here
}


void saveConfig(){
    save(CONFIG_FILENAME);
}

void save(const char * _fileName){
    // Serial.printf("saving %s\n", _fileName);
    // if(sd.exists(_fileName)){
    //     sd.remove(_fileName);
    // }
    // File _file = sd.open(_fileName, FILE_WRITE);
    //
    // StaticJsonDocument<JSON_BUFFER_SIZE> doc;
    // saveParams(doc, paramCollector);
    //
    // serializeJson(doc, _file);
    // serializeJsonPretty(doc, Serial);
    // Serial.println();
    // _file.close();
}

void load(const char * _fileName){
    // File _file = sd.open(_fileName);
    // StaticJsonDocument<JSON_BUFFER_SIZE> doc;
    // DeserializationError err = deserializeJson(doc, _file);
    // if(err){
    //     Serial.printf("json fail :%s \n", err.c_str());
    // }
    // // JsonObject obj = doc[]
    // // serializeJsonPretty(doc, Serial);
    // loadParams(doc, paramCollector);
    // _file.close();
}




void bangFunction(){
    Serial.println("bang");
}

void ledCallback(bool i){
    Serial.printf("bool : %i\n", i);
    digitalWrite(LED_PIN, i);
}

void floatCallback(float f) {
    Serial.printf("float : %f\n", f);
}

void intCallback(int i) {
    Serial.printf("int : %i\n", i);
}

void colorCallback(long _i) {
    Serial.printf("color : %i\n", _i);
}

void textCallback(const char * _t) {
    Serial.printf("text : %s\n", _t);
}

void enumCallback(uint16_t _v) {
    Serial.printf("enum : %i %s\n", _v, enumStrings[_v]);
}
