/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#include "Param.h"

void Param::set(ParamAddress _address){
    address = _address;
}

bool Param::match(ParamAddress _address) {
    return (strcmp(_address, address) == 0);
}

bool Param::checkFlag(){
    if(flag) {
        flag = false;
        return true;
    }
    else {
        return false;
    }
}

void Param::setBoolValue(bool _i){}
void Param::setFloatValue(float _i){}
void Param::setIntValue(int _i){}
void Param::setColorValue(long _i){}
void Param::setTextValue(const char* _i){}
void Param::setEnumValue(int _i){}
void Param::setBangValue(){}
void Param::setWithByteValue(uint8_t _v){}

void Param::receiveByte(uint8_t _v){}
void Param::receiveBang(){}
void Param::receiveBool(bool _b){}

bool Param::hasChanged(){return false;}

uint8_t Param::getByteValue(){return 0;}
//////////////////////////////////////////////////////////////////////////////


void BoolParam::set(ParamAddress _address, bool _def){
    address = _address;
    def = _def;
    v = def;
    previous = v;
    inputType = TOGGLE;
    paramType = BOOL_PARAM;
}

void BoolParam::setBoolValue(bool _v){
    previous = v;
    v = _v;
    flag = true;
    if(callback) {
        callback(v);
    }
}

bool BoolParam::hasChanged(){
    if(v != previous) {
        previous = v;
        return true;
    }
    else {
        return false;
    }
}

void BoolParam::setWithByteValue(uint8_t _v) {
    if( (_v!=0) != v) setBoolValue(_v != 0);
}
uint8_t BoolParam::getByteValue(){
    return v;
}

void BoolParam::receiveByte(uint8_t _v){
    if( (_v!=0) != v) setBoolValue(_v != 0);
}

void BoolParam::receiveBang(){
    setBoolValue(!v);
}

void BoolParam::receiveBool(bool _b){
    setBoolValue(_b);
}

//////////////////////////////////////////////////////////////////////////////


void FloatParam::set(ParamAddress _address, float _min, float _max, float _def){
    address = _address;
    min = _min;
    max = _max;
    def = _def;
    v = def;
    previous = v;
    inputType = F_SLIDER;
    paramType = FLOAT_PARAM;
}

void FloatParam::setFloatValue(float _v){
    flag = true;
    if(_v > max) _v = max;
    else if(_v < min) _v = min;
    previous = v;
    v = _v;
    if(callback) {
        callback(v);
    }
}

bool FloatParam::hasChanged(){
    if(v != previous) {
        previous = v;
        return true;
    }
    else {
        return false;
    }
}

void FloatParam::setWithByteValue(uint8_t _v) {
    float newVal = ((float)_v/255.0) * (max-min) + min;
    if(newVal != v) setFloatValue(newVal);
}

uint8_t FloatParam::getByteValue(){
    return ((v-min)/(max-min))*255;
}

void FloatParam::receiveByte(uint8_t _v){
    float newVal = ((float)_v/255.0) * (max-min) + min;
    if(newVal != v) setFloatValue(newVal);
}

void FloatParam::receiveBang(){
    float newVal = v+(max-min)/10;
    setFloatValue(newVal < max ? newVal : min);
}

void FloatParam::receiveBool(bool _b){
    setFloatValue(_b?def:min);
}
//////////////////////////////////////////////////////////////////////////////


void IntParam::set(ParamAddress _address, int _min, int _max, int _def){
    address = _address;
    v = _def;
    min = _min;
    max = _max;
    def = _def;
    inputType = I_SLIDER;
    paramType = INT_PARAM;
}

void IntParam::setIntValue(int _v){
    flag = true;
    if(_v > max) _v = max;
    else if(_v < min) _v = min;
    previous = v;
    v = _v;
    if(callback) {
        callback(v);
    }
}

bool IntParam::hasChanged(){
    if(v != previous) {
        previous = v;
        return true;
    }
    else {
        return false;
    }
}

void IntParam::setWithByteValue(uint8_t _v) {
    int newVal = ((float)_v/255.0) * (max-min) + min;
    if(newVal != v) setIntValue(newVal);
}
uint8_t IntParam::getByteValue(){
    return (float(v-min)/float(max-min))*255;
}

void IntParam::receiveByte(uint8_t _v){
    int newVal = ((float)_v/255.0) * (max-min) + min;
    if(newVal != v) setIntValue(newVal);
}

void IntParam::receiveBang(){
    setIntValue((v+1/*(max-min)/10*/)%max);
}

void IntParam::receiveBool(bool _b){
    setFloatValue(_b?def:min);
}
//////////////////////////////////////////////////////////////////////////////

void ColorParam::set(ParamAddress _address, long _def){
    address = _address;
    v = _def;
    previous = v;
    def = _def;
    inputType = COLOR;
    paramType = COLOR_PARAM;
}

void ColorParam::setColorValue(long _v){
    previous = v;
    v = _v;
    flag = true;

    if(callback) {
        callback(v);
    }
}

bool ColorParam::hasChanged(){
    if(v != previous) {
        previous = v;
        return true;
    }
    else {
        return false;
    }
}

// how to set with DMX? add a set red, set green, set blue?
void ColorParam::setWithByteValue(uint8_t _v){
    setColorValue(_v);
}

void ColorParam::setBlue(int _c){
    v &= 0xFF0000;
    v |= _c;
}
void ColorParam::setGreen(int _c){
    v &= 0x00FF00;
    v |= _c << 8;
}
void ColorParam::setRed(int _c){
    v &= 0x0000FF;
    v |= _c << 16;
}
uint8_t ColorParam::getByteValue(){
    return 0;
}
// void ColorParam::receiveByte(uint8_t _v){
//     setColorValue(_v);
// }
// void ColorParam::receiveBang(){
//     // random color?
//     setColorValue(random(16777215));
// }
// void ColorParam::receiveBool(bool _b){
//     setColorValue(_b?def:0);
// }

//////////////////////////////////////////////////////////////////////////////

void TextParam::set(ParamAddress _address, const char* _def){
    address = _address;
    strcpy(def, _def);
    strcpy(previous, _def);
    setTextValue(def);
    inputType = TEXT;
    paramType = TEXT_PARAM;
}

void TextParam::setTextValue(const char* _v){
    flag = true;
    
    if(strlen(_v) > TEXT_PARAM_MAX_LENGTH){
        Serial.println("[param] err- text param truncated");
        memcpy(previous, v, TEXT_PARAM_MAX_LENGTH);
        memcpy(v, _v, TEXT_PARAM_MAX_LENGTH);
    }
    else {
        strcpy(v, _v);
    }
    if(callback) {
        callback(v);
    }
}

bool TextParam::hasChanged(){
    if(strcmp(v, previous) != 0) {
        memcpy(previous, v, TEXT_PARAM_MAX_LENGTH);
        return true;
    }
    else {
        return false;
    }
}

void TextParam::setWithByteValue(uint8_t _v){
    // setTextValue(&_v);
}
uint8_t TextParam::getByteValue(){
    return v[0];
}
//////////////////////////////////////////////////////////////////////////////

void EnumParam::set(ParamAddress _address, int _def){
    address = _address;
    v = def;
    def = _def;
    previous = v;
    inputType = OPTION;
    paramType = ENUM_PARAM;
}

void EnumParam::setOptions(EnumOption * _options, size_t _s){
    options = _options;
    optionCount = _s;
    for(int i = 0; i < optionCount; i++) {
        if(def == options[i].value) {
            defIndex = i;
        }
        if(v == options[i].value){
            index = i;
        }
    }
}

// set by not the index but the value of the enum
void EnumParam::setEnumValue(int _v){
    for(int i = 0; i < optionCount; i++){
        if(_v == options[i].value){
            index = i;
            previous = v;
            v = _v;
            flag = true;
            if(callback) callback(v);
            break;
        }
    }
}
// if we want to set with a string, for serialization/deserialization
void EnumParam::setTextValue(const char* _key){
    for(int i = 0; i < optionCount; i++) {
        if(strcmp(options[i].name, _key) == 0){
            setEnumValue(options[i].value);
            break;
        }
    }
}

bool EnumParam::hasChanged(){
    if(v != previous) {
        previous = v;
        return true;
    }
    else {
        return false;
    }
}

void EnumParam::setWithByteValue(uint8_t _i) {
    if(_i < optionCount) {
        setEnumValue(options[_i].value);
    }
}

void EnumParam::receiveByte(uint8_t _i){
    _i = map(_i, 0, 255, 0, optionCount-1);
    setWithByteValue(_i);
}

void EnumParam::receiveBang(){
    if(index+1 >= optionCount) {
        index = 0;
    }
    else {
        index++;
    }
    setEnumValue(options[index].value);
}

void EnumParam::receiveBool(bool _b){
    setEnumValue(options[_b?defIndex:0].value);
}

// note we return the index here and not the value
uint8_t EnumParam::getByteValue(){
    return index;
}

//////////////////////////////////////////////////////////////////////////////

void BangParam::set(ParamAddress _address){
    address = _address;
    inputType = BANG;
    paramType = BANG_PARAM;
    flag = false;
}

void BangParam::setBangValue(){
    if(callback) callback();
    flag = true;
}

void BangParam::setWithByteValue(uint8_t _v) {
    setBangValue();
}
uint8_t BangParam::getByteValue(){
    return flag;
}

void BangParam::receiveByte(uint8_t _v){setBangValue();}
void BangParam::receiveBang(){setBangValue();}
void BangParam::receiveBool(bool _b){setBangValue();}

//////////////////////////////////////////////////////////////////////////////

// template <class T>
// void NumParam<T>::set(ParamAddress _address, T _min, T _max, T _def){
//     address = _address;
//     v = _def;
//     min = _min;
//     max = _max;
//     def = _def;
//     inputType = SLIDER;
//
// }
//
// template <class T>
// void NumParam<T>::setValue(T _v){
//     v = _v;
// previous = _v;
//     if(callback) {
//         callback(v);
//     }
// }
//
// template class NumParam<int>;
// template class NumParam<float>;
// template class NumParam<double>;
// template class NumParam<uint8_t>;
// template class NumParam<uint16_t>;
