/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2020-02-11
 */

#ifndef PARAM_UTILS_H
#define PARAM_UTILS_H

#include "Param.h"
#include "ParamCollector.h"


/**
 * @class ParamLinker
 * @brief Utility to control a param according to a defined address
 *
 * control arbitrary parameters by using a TextParam to set the address of the
 * param we wish to control. With this we can assign any parameter to a button or pot,
 * during runtime.
 */
class ParamLinker {
public:
    TextParam paramAddress;
    Param * targetParam;
    /**
     * @brief Check if param address has changed
     * @return true if param address has changed
     */
    bool needsUpdate();
    /**
     * @brief Find param by address and store the pointer
     * @param _pc param collector
     * @return true if param was found
     */
    bool findParam(ParamCollector * _pc);
    /**
     * @brief apply a byte value to the target param
     * @param _v byte value
     */
    void receiveByte(byte _v);
    /**
     * @brief apply a bang value to the target param
     */
    void receiveBang();
    /**
     * @brief apply a bool value to the target param
     * @param _b bool value
     */
    void receiveBool(bool _b);
    /**
     * @brief check if a param is linked
     * @return true if param is linked
     */
    bool isLinked();
    
    byte value;
    bool invert = false;
    // void receiveFloat(float _f);
    // void receiveInt(int _v);
};


#endif
