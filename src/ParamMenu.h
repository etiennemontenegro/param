/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.0.1
 * @since     2020-02-11
 */

#ifndef PARAM_MENU_H
#define PARAM_MENU_H

#include "Param.h"
#include "ParamCollector.h"

#define MAX_ITEMS 128
#define PARAM_MENU_MAX_NAME_LENGTH 12
struct MenuItem {
    char name[PARAM_MENU_MAX_NAME_LENGTH];
    // pointers
    MenuItem * parent;
    MenuItem * previous;
    MenuItem * next;
    MenuItem * child;
    // type? item, param, return/back
    Param * param;
};


class ParamMenu {
public:
    void begin(ParamCollector _collector);
    // allocates staticaly
    MenuItem allItems[MAX_ITEMS];
    int index;
    MenuItem * selectedItem;
    MenuItem * root;
    MenuItem * nullItem;
    int depth;
    void up();
    void down();
    void enter();
    void reset();
    void addParam(Param * _p);
    bool update;
};



class MenuRenderer {
public:
    int width;
    int height;
    Print * printer;
    void begin(int w, int h, Print * p);
    void render(ParamMenu menu);
    void recursivePrint(MenuItem * item, int depth);
    void printOutMenu(ParamMenu menu);
};


#endif
