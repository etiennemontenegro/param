/**
 * ##copyright##
 * See LICENSE
 *
 * @author    Maxime Damecour (http://nnvtn.ca)
 * @version   0.1.0
 * @since     2019-09-01
 */

#ifndef PARAMOSC_H
#define PARAMOSC_H

#include <MicroOsc.h>
#include <Arduino.h>

#include "Param.h"
#include "ParamCollector.h"

/**
 * @brief Dispatch OSC input to params
 * 
 * @param _message MicroOSC message
 * @param _collector ParamCollector to dispatch intput to
 * @return true if param was found
 */
bool paramDispatchOSC(MicroOscMessage& _message, ParamCollector * _collector);

// some sort of
// void pushParameters(ParamCollector _collector, IPAddress, port, UdpSomething)

#endif
