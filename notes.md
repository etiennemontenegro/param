https://hackernoon.com/a-simple-introduction-to-pythons-asyncio-595d9c9ecf8c
https://tinkering.xyz/async-serial/


### String abolition
https://forum.arduino.cc/index.php?topic=63659.msg463025#msg463025
```
char * foo ()
{
char * buf = (char *) malloc (666);
strcpy (buf, "But does it get goat's blood out?");
return buf;
}  // end of foo

void setup ()
{
  Serial.begin (115200);
  char * p = foo ();
  Serial.println (p);
  free (p);
}  // end of setup

void loop () {}
```

Maybe a macro could work?
Maybe I could just write a interface that takes a Parameter and makes a arduinoJSON object.

ESP8266 Flash thing...
http://marrin.org/2017/01/16/putting-data-in-esp8266-flash-memory/


### Struct style
```
struct Param {
    int type;
    void* variablePointer;

}
```


https://github.com/rabbitControl/RCP/blob/master/RCPSpecification.md

https://github.com/Vidvox/OSCQueryProposal


### Scheduler
`-1` means every, makes sense to only have it on the lowest denominator.
```
{"year":-1, "month":-1, "day":-1, "hour":-1, "minute":-1, "second":10  }
```
weekly reminder
```
{"weekday":"Wednesday", "hour":4, "minute":20}
```
daily lunch alarm
```
[
{"day":-1, "hour":11, "minute":59, "event":{"addr":"/foo/ha", "v":4}},
{"minute":-1, "seconds":10, "event":{"addr":"/foo/ha", "v":4}}
]
```



https://stackoverflow.com/questions/29881957/websocket-connection-timeout
